{
  'targets': [
    {
    # This is a header only library
      'configurations': {
        'Debug': { },
        'Release': { }
      },
      'target_name': 'rtaudio',
      'type': 'static_library',
      
      'include_dirs' : [ 
        '../third_party/rtaudio',
        '../third_party/rtaudio/include'
      ],
      'sources' : [ 
        '../third_party/rtaudio/RtAudio.cpp',
        '<!@(ls -1 ../third_party/rtaudio/include/*.cpp)'
      ],
      
      'conditions': [
        ['OS=="win"', {
          'link_settings': {
            'libraries' : [
              '-lDxErr.lib',
              '-ldsound.lib',
              '-ldxguid.lib',
              '-lwinmm.lib',
            ],
            'library_dirs': [
              '<!(echo %DXSDK_DIR%)\\Lib\\x64',
            ],
          },
          'defines': [
            '__WINDOWS_ASIO__',
            '__WINDOWS_DS__',
            '__WINDOWS_WASAPI__',
            'WIN32', # Rt not usually built with VCC I guess. VCC defines _WIN32, not WIN32.
          ],
          
          'msvs_settings': {
            'VCCLCompilerTool': {
                'AdditionalOptions': [ '/EHsc' ],
            },
          },
        }],
      ], # End conditions

      'direct_dependent_settings': {
        'include_dirs': [
          '../third_party/rtaudio',
        ],
      },
    }
  ]
}
