{
  'targets': [
    {
    # This is a header only library
      'configurations': {
        'Debug': { },
        'Release': { }
      },
      'target_name': 'vc',
      'type': 'none',
#      'include_dirs':[
#        '../third_party/vc',
#        '../third_party/vc/include',
#      ],
      'direct_dependent_settings': {
        'include_dirs': [
          '../third_party/vc/include',
          '../third_party/vc',
        ],
        'defines': [
#          'SIMDPP_ARCH_X86_SSE4_1'
        ]
      },

      'msvs_settings': {
        'VCCLCompilerTool': {
            'AdditionalOptions': [ '/showIncludes' ],
        },
      },
    }
  ]
}