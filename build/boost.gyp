{
  'targets': [
    {
      'configurations': {
        'Debug': { },
        'Release': { }
      },
      'target_name': 'boost',
      'type': 'none',
      'include_dirs': [ '<!(echo $BOOST_ROOT)' ],
    }
  ]
}