{
  'targets': [
    {
    # This is a header only library
      'configurations': {
        'Debug': { },
        'Release': { }
      },
      'target_name': 'libsimdpp',
      'type': 'none',
      'direct_dependent_settings': {
        'include_dirs': [
          '../third_party/libsimdpp',
        ],
        'defines': [
          'SIMDPP_ARCH_X86_SSE4_1'
        ]
      },
    }
  ]
}