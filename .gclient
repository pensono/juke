solutions = [
  { "name"        : ".",
    "url"         : "git@bitbucket.org:pensono/juke.git",
    "deps_file"   : "DEPS",
    "managed"     : True,
    "custom_deps" : {
    },
    "safesync_url": "",
  },
  { "name"        : "third_party/skia",
    "url"         : "git@bitbucket.org:pensono/skia.git",
    "deps_file"   : "DEPS",
    "managed"     : True,
    "custom_deps" : {
    },
    "safesync_url": "",
  },
]
cache_dir = None
