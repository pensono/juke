{
  'variables': {
#    'skia_use_sdl': '1',
  },
  'includes': [
    'third_party/skia/gyp/apptype_console.gypi',
  ],
  'targets': [
    {
      'configurations': {
        'Debug': { },
        'Release': { }
      },
      'target_name': 'juke',
      'type': 'executable',
      'dependencies': [
        'third_party/skia/gyp/icu.gyp:icuuc',
        'third_party/skia/gyp/skia_lib.gyp:skia_lib',
        'third_party/skia/gyp/views.gyp:views',
        'build/rtaudio.gyp:rtaudio',

        #Let's begin to use the amazing Vc library once it is supported by MSVC
        #'build/vc.gyp:vc',
      ],
      'include_dirs': [ '<!@(ls -1dr third_party/skia/include/*)', ],
      'sources' : [ '<!@(ls -1 juke/*.cpp)' ],
            
      'msvs_settings': {
        'VCCLCompilerTool': {
            'AdditionalOptions': [ '/EHsc', '/arch:AVX' ],
            #https://github.com/electron/gyp/blob/master/test/win/compiler-flags/enable-enhanced-instruction-set.gyp
            #'EnableEnhancedInstructionSet ': '2'
        },       
        'VCLinkerTool': {
          #Allows for creation / output to console.
          #Console (/SUBSYSTEM:CONSOLE)
          'SubSystem': '1',

          #Console app, use main/wmain
          'EntryPointSymbol': 'mainCRTStartup',
        },
      }
    }
  ]
}
