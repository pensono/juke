#ifndef LAYOUT_H_DEFINED
#define LAYOUT_H_DEFINED

#include <vector>
#include <memory>
#include "Component.h"

namespace juke
{
	struct ComponentLayout {
		std::unique_ptr<Component> component;
		Point location;
	};

	class Layout : public Component {
	public:
		Layout();
		~Layout();

		void draw(SkCanvas* canvas) override;

		virtual void layoutComponents() = 0;

		void addChild(std::unique_ptr<Component> component);

		std::vector<ComponentLayout>& children() { return components; }

		bool dispatchMouseEvent(InputEvent event) override;

		Component* focusedComponent() override;
		void setFocused(bool) override;
		//void advanceFocus(FocusDirection);

	protected:
		void onSizeChange() override {
			layoutComponents();
		}
		virtual void onChildAdded() {}

	private:
		std::vector<ComponentLayout> components;
	};
}
#endif //include guard
