#ifndef JTYPES_H_DEFINED
#define JTYPES_H_DEFINED

#include <algorithm>

namespace juke
{
	template< typename Numerator, typename Denominator>
	class Ratio {
		float value;

	public:
		constexpr Ratio(Numerator numerator, Denominator denominator) : value(numerator / denominator) {}

		constexpr operator float() const { return value; }

		constexpr Ratio<Denominator, Numerator> inverse() const { return Ratio{ 1 / value }; }
	};

	//Because C++ doesn't have constructor template infrence :( Thanks Bjarne.
	template<typename Numerator, typename Denominator>
	static constexpr Ratio<Numerator, Denominator> mkRatio(Numerator numerator, Denominator denominator) {
		return Ratio<Numerator, Denominator>{numerator, denominator };
	}

	class Proportion {
		float value;

	public:
		constexpr Proportion(float value) : value(value) {}

		constexpr operator float() const { return value; }

		constexpr Proportion operator-(Proportion other) const { return Proportion{ value - other.value }; }
		constexpr Proportion operator+(Proportion other) const { return Proportion{ value + other.value }; }
		constexpr Proportion operator*(Proportion other) const { return Proportion{ value * other.value }; }

		Proportion& operator +=(const Proportion& b) {
			value += b.value;
			return *this;
		}
		Proportion& operator -=(const Proportion& b) {
			value -= b.value;
			return *this;
		}

		constexpr Proportion inverse()    const { return Proportion{ 1 / value }; }
		constexpr Proportion compliment() const { return Proportion{ 1 - value }; }
	};

	//Move these to be inside the Proportion class when it's allowed in C++
	/*static constexpr Proportion Whole;
	static constexpr Proportion Half ;
	static constexpr Proportion None ;
	*/
	constexpr Proportion Whole = Proportion{ 1.0f };
	constexpr Proportion Half =  Proportion{ 0.5f };
	constexpr Proportion None =  Proportion{ 0.0f };


	template <typename T>
	T constexpr clamp(const T n, const T lower, const T upper) {
		return std::max(lower, std::min(n, upper));
	}
}

#endif //include guard