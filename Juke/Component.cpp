#include "Component.h"

using namespace juke;

Component::Component(StateMachine machine) : _focused(false), _parent(nullptr), _stateMachine(machine) {}

Component::~Component() {}

void Component::setFocused(bool focus) {
	bool oldFocus = _focused;
	if (focus && !_focused && _parent)
		_parent->setFocused(true); //Pass a grab of focus op the component hierarchy

	_focused = focus;

	if (_focused != oldFocus)
		onFocusChange();
}

Component* Component::focusedComponent() {
	return _focused ? this : nullptr;
}

//void Component::advanceFocus(FocusDirection direction) {
//	if (_parent)
//		_parent->advanceFocus(direction);
//	_focused = false;
//}