#ifndef COLORS_H_DEFINED
#define COLORS_D_DEFINED

#include "SkColor.h"

namespace juke
{
	namespace Color {
		static constexpr SkColor Accent = 0xFF2F5777;
		static constexpr SkColor Text = 0xFFF4F4F4;
		static constexpr SkColor Light = 0xFF505050;
		static constexpr SkColor Dark = 0xFF262626;
		static constexpr SkColor Primary = 0xFF404040;
		static constexpr SkColor Hover = 0xFF383838;
	};
}

#endif //include guard