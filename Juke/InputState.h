#ifndef CLICK_H_DEFINED
#define CLICK_H_DEFINED

#include "SkView.h"
#include "Geometry.h"
#include "enable_bitmask_operators.h"

namespace juke
{
	enum class MouseButton {
		None = 0,
		Left = 1,
		Middle = 2,
		Right = 4,
		X1 = 8,
		X2 = 16,
	};

	template<>
	struct enable_bitmask_operators<MouseButton> {
		static constexpr bool enable = true;
	};

	class InputState {
	public:
		constexpr InputState(const Point location, const MouseButton button) : _location(location), _buttons(button) {}

		constexpr Point const location() const { return _location; }
		constexpr bool buttonPressed(MouseButton b) const { return test(_buttons, b); }

		constexpr InputState const translate(Vector delta) const { return InputState{ _location + delta, _buttons }; }
		constexpr InputState const withLocation(Point location) const { return InputState{ location, _buttons }; };
		constexpr InputState const withButtonDown(MouseButton button) const { return InputState{ _location, set(_buttons, button) }; };
		constexpr InputState const withButtonUp(MouseButton button) const { return InputState{ _location, clear(_buttons, button) }; };
		
	//private:
		Point _location;
		MouseButton _buttons;
	};

	class InputEvent {
	public:
		constexpr InputEvent(const InputState now, const InputState then) : _now(now), _then(then) {}

		constexpr InputState const now() const { return _now; }
		constexpr InputState const then() const { return _then; }

		constexpr Vector const movement() const { return _now.location() - _then.location(); }

		constexpr InputEvent const translate(Vector delta) const { return InputEvent{ _now.translate(delta), _then.translate(delta) }; }

	private:
		InputState _now;
		InputState _then;
	};
}

#endif