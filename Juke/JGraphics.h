#ifndef J_GRAPHICS_H
#define J_GRAPHICS_H

#include "unicode/unistr.h"
#include "Geometry.h"

class SkCanvas;

namespace juke
{
	enum class HorizontalAlignment {
		Left,
		Center,
		Right
	};

	enum class VerticalAlignment {
		Top,
		Center,
		Bottom,
	};

	void drawText(SkCanvas* canvas, icu::UnicodeString text, Rect bounds, HorizontalAlignment horizontal, VerticalAlignment vertical);
}

#endif // Include Guard