/*
 * Copyright 2015 Google Inc.
 *
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 *
 */

#ifndef HelloWorld_DEFINED
#define HelloWorld_DEFINED

#include <memory>
#include "SkSurface.h"
#include "SkWindow.h"
#include "Geometry.h"
#include "InputState.h"

class GrContext;
struct GrGLInterface;
class GrRenderTarget;
class SkCanvas;

namespace juke
{
	class Layout;

	class Window : public ::SkOSWindow {
	public:
		Window(void* hwnd);
		virtual ~Window() override;
		
	protected:
		SkSurface* createSurface() override {
			SkSurfaceProps props(INHERITED::getSurfaceProps());
			return SkSurface::MakeRenderTargetDirect(fRenderTarget, &props).release();
		}

		void draw(SkCanvas* canvas) override;
		void onSizeChange() override;
		bool onDispatchClick(int x, int y, Click::State, Click::Button, void* owner, unsigned modi) override;

	private:
		bool setUpBackend();
		void setUpRenderTarget();
		bool onHandleChar(SkUnichar unichar) override;
		void tearDownBackend();

		std::unique_ptr<juke::Layout> content;

		// support frameworkSkSurface* fSurface;
		GrContext* fContext;
		GrRenderTarget* fRenderTarget;
		AttachmentInfo fAttachmentInfo;
		const GrGLInterface* fInterface;

		MouseButton buttonState;
		InputState previousEvent;

		typedef SkOSWindow INHERITED;
	};

#endif
}