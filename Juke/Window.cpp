/*
 * Copyright 2015 Google Inc.
 *
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 *
 */

#include "Window.h"

#include "gl/GrGLInterface.h"
#include "GrContext.h"
#include "SkApplication.h"
#include "SkCanvas.h"
#include "SkGradientShader.h"
#include "SkGraphics.h"
#include "SkGr.h"
#include "Colors.h"
#include "FlowLayout.h"

#include "Button.h"
#include "Knob.h"
#include "Slider.h"

#include "AudioTest.h"

using namespace juke;

void application_init() {
	SkGraphics::Init();
	SkEvent::Init();

	AudioTest test;
	test.start();
}

void application_term() {
	SkEvent::Term();
}

Window::Window(void* hwnd)
	: INHERITED(hwnd), previousEvent(Point{}, MouseButton::None) {
	fRenderTarget = NULL;
	this->setTitle("Juke Window");
	this->setUpBackend();

	content = std::make_unique<FlowLayout>();
	content->addChild(std::make_unique<Button>("Hello Skia 1", [] { printf("Click 1\n");}));
	content->addChild(std::make_unique<Button>("Hello Skia 2", [] { printf("Click 2\n");}));
	content->addChild(std::make_unique<Button>("Hello Skia 3", [] { printf("Click 3\n");}));
	content->addChild(std::make_unique<Button>("Hello Skia 4", [] { printf("Click 4\n");}));
	content->addChild(std::make_unique<Button>("Hello Skia 5", [] { printf("Click 5\n");}));
	content->addChild(std::make_unique<Knob>("Cutoff"));
	content->addChild(std::make_unique<Slider>("Gain"));
}

Window::~Window() {
	tearDownBackend();
}

void Window::tearDownBackend() {
	SkSafeUnref(fContext);
	fContext = NULL;
	SkSafeUnref(fInterface);
	fInterface = NULL;

	SkSafeUnref(fRenderTarget);
	fRenderTarget = NULL;

	INHERITED::release();
}


void Window::draw(SkCanvas* canvas) {
	canvas->drawColor(Color::Primary);
	content->draw(canvas);
	fContext->flush();
	inval(NULL); // Replace with something simmilar to SampleApp.cpp:1532 per component that needs animation.
	fRenderTarget->flushWrites();
	present();
}

bool Window::setUpBackend() {
	this->setVisibleP(true);
	this->setClipToBounds(false);

	bool result = attach(kNativeGL_BackEndType, 0 /*msaa*/, &fAttachmentInfo);
	if (false == result) {
		SkDebugf("Not possible to create backend.\n");
		release();
		return false;
	}

	fInterface = GrGLCreateNativeInterface();

	SkASSERT(NULL != fInterface);

	fContext = GrContext::Create(kOpenGL_GrBackend, (GrBackendContext)fInterface);
	SkASSERT(NULL != fContext);

	this->setUpRenderTarget();
	return true;
}

void Window::setUpRenderTarget() {
	SkSafeUnref(fRenderTarget);
	fRenderTarget = this->renderTarget(fAttachmentInfo, fInterface, fContext);
}

void Window::onSizeChange() {
	setUpRenderTarget();
	content->setSize(Size{ width(), height() });
}

bool Window::onHandleChar(SkUnichar unichar) {
	return true;
}

bool Window::onDispatchClick(int x, int y, Click::State state, Click::Button button, void* owner, unsigned modifierKeys) {
	InputState currentEvent = previousEvent;
	switch (state) {
	case Click::State::kMoved_State:
		currentEvent = currentEvent.withLocation(Point{ Measure{ (float)x }, Measure{ (float)y } });
		break;
	case Click::State::kUp_State:
		currentEvent = currentEvent.withButtonUp((MouseButton) (1 << (button-1)));
		break;
	case Click::State::kDown_State:
		currentEvent = currentEvent.withButtonDown((MouseButton) (1 << (button-1)));
		break;
	} 
	 
	content->dispatchMouseEvent(InputEvent{ currentEvent, previousEvent });
	
	previousEvent = currentEvent;
	return false;
}

SkOSWindow* create_sk_window(void* hwnd, int , char** ) {
	return new Window(hwnd);
}
