#include "SkCanvas.h"
#include "Colors.h"
#include "JGraphics.h"
#include "Slider.h"

using namespace juke;

Slider::Slider(icu::UnicodeString label) :
	Component(StateMachine{ SliderState::Inactive, {
		State{ SliderState::Inactive, {
			StateTransition{ MouseEnter(this), SliderState::Hover }
		}},
		State{ SliderState::Hover,{
			StateTransition{ MouseExit(this), SliderState::Inactive },
			StateTransition{ MouseDown(MouseButton::Left), SliderState::Edit, [=](InputEvent e) { doEdit(e); setFocused(true);} }
		} },
		State{ SliderState::Edit,{
			StateTransition{ And(MouseUp(MouseButton::Left), MouseInside(this)), SliderState::Hover },
			StateTransition{ And(MouseUp(MouseButton::Left), Not(MouseInside(this))), SliderState::Inactive },
			StateTransition{ MouseMove, SliderState::Edit, [=](InputEvent e) { doEdit(e);} },
		} },
	} } ), label(label) {}

Slider::~Slider() {}

Size Slider::minimumSize() {
	return Size{ 24, 100 };
}

void Slider::doEdit(InputEvent e) {
	auto locationOnTrack = e.now().location().y() - sliderHeight * Half;
	value = locationOnTrack * mkRatio(Whole, size().height() - bottomHeight - sliderHeight);
	value = clamp(value, None, Whole);
}

void Slider::draw(SkCanvas* canvas) {
	Size upper = Size{ size().width(), size().height() - bottomHeight };
	auto trackWidth = stateMachine()->currentState() == SliderState::Inactive ? trackWidthInactive : trackWidthActive;
	Rect track = Size{ trackWidth, upper.height() - sliderHeight }.centerWithin(upper);

	//Draw the accented track first, then the normal part over to cover it up
	SkPaint trackPaint;
	trackPaint.setColor(Color::Accent);
	canvas->drawRect(track, trackPaint);
	trackPaint.setColor(Color::Dark);
	canvas->drawRect(Rect{ track.location(),Size{ track.width(), track.height() * value} }, trackPaint);

	drawText(canvas, label, Rect{ size() }, HorizontalAlignment::Center, VerticalAlignment::Bottom);

	SkPaint sliderPaint = trackPaint;
	canvas->drawRect(Rect{ Point{0, (upper.height() - sliderHeight) * value}, Size{size().width(), sliderHeight} }, sliderPaint);
}
