#include <stdio.h>
#include <vector>
#include "Button.h"
#include "SkCanvas.h"
#include "SkTypeface.h"
#include "Colors.h"
#include "JGraphics.h"

using namespace juke;

Button::Button(icu::UnicodeString label, std::function<void()> action) :
	Component(StateMachine{ ButtonState::Inactive,{
		State{ ButtonState::Inactive, {
			StateTransition{ MouseEnter(this), ButtonState::Hover },
		} },
		State{ ButtonState::Hover, {
			StateTransition{ MouseExit(this), ButtonState::Inactive },
			StateTransition{ MouseDown(MouseButton::Left), ButtonState::Pressed, [=](InputEvent c) { action(); setFocused(true);} },
		} },
		State{ ButtonState::Pressed, {
			StateTransition{ And(MouseUp(MouseButton::Left), MouseInside(this)), ButtonState::Hover},
			StateTransition{ And(MouseUp(MouseButton::Left), Not(MouseInside(this))), ButtonState::Inactive },
		} },
	} }),
	label(label), action(action)
	 {}

Button::~Button() {}

Size Button::minimumSize() {
	return Size{ 100, 24 };
}

void Button::draw(SkCanvas* canvas) {
	SkColor backgroundColor;
	switch (stateMachine()->currentState()) {
	case ButtonState::Hover:
		backgroundColor = Color::Light; break;
	case ButtonState::Inactive:
		backgroundColor = Color::Dark; break;
	case ButtonState::Pressed:
		backgroundColor = Color::Accent; break;
	}

	SkPaint rectPaint;
	rectPaint.setColor(backgroundColor);
	Rect rect = Rect{ size() };
	canvas->drawRect(rect, rectPaint);

	if (isFocused()) {
		rectPaint.setColor(Color::Accent);
		rectPaint.setStyle(SkPaint::kStroke_Style);
		canvas->drawRect(rect, rectPaint);
	}

	//Label
	drawText(canvas, label, rect, HorizontalAlignment::Center, VerticalAlignment::Center);
}