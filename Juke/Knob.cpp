#include "SkCanvas.h"
#include "JGraphics.h"
#include "Knob.h"
#include "Colors.h"

using namespace juke;

Knob::Knob(icu::UnicodeString label) :
	Component(StateMachine{ KnobState::Inactive,{
		State{ KnobState::Inactive, {
			StateTransition{ MouseEnter(this), KnobState::Hover},
		}},
		State{ KnobState::Hover,{
			StateTransition{ MouseExit(this), KnobState::Inactive},
			StateTransition{ MouseDown(MouseButton::Left), KnobState::Edit, [=](InputEvent e) {setFocused(true);} },
		} },
		State{ KnobState::Edit,{
			StateTransition{ And(MouseUp(MouseButton::Left), MouseInside(this)), KnobState::Hover },
			StateTransition{ And(MouseUp(MouseButton::Left), Not(MouseInside(this))), KnobState::Inactive },
			StateTransition{ MouseMove, KnobState::Edit, [=](InputEvent e) { doEdit(e);} },
		} },
	} }	),
	_value(0.75f), label(label) {}

Knob::~Knob() {}

Size Knob::minimumSize() {
	return Size{ 48, 56 };
}

void Knob::doEdit(InputEvent state) {
	_value -= state.movement().dy() * mkRatio(Proportion{ .03f }, Measure{ 5 });
	_value = clamp(_value, None, Whole);
}

void Knob::draw(SkCanvas* canvas) {
	auto span = 285_deg;
	auto start = 90_deg +(360_deg - span) * Half;
	auto knobRect = Rect{ Size{size().width(), size().width()} }.inset(4);

	SkPaint arcPaint;
	arcPaint.setColor(Color::Dark);
	arcPaint.setStyle(SkPaint::kStroke_Style);
	arcPaint.setAntiAlias(true);
	arcPaint.setStrokeWidth(stateMachine()->currentState() != KnobState::Inactive ? 4.0f : 3.25f);
	canvas->drawArc(knobRect, start, span, false, arcPaint);

	arcPaint.setColor(Color::Accent);
	canvas->drawArc(knobRect, start, span * _value, false, arcPaint);

	// Label
	drawText(canvas, label, Rect{ size() }, HorizontalAlignment::Center, VerticalAlignment::Bottom);
}