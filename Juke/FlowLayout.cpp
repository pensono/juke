#include <algorithm>
#include "FlowLayout.h"

using namespace juke;

FlowLayout::FlowLayout() {}

FlowLayout::~FlowLayout() {}

void FlowLayout::layoutComponents() {
	Measure nextX;
	Measure nextY;
	Measure nextLine;
	for (auto& componentLayout : children()) {
		Size componentSize = componentLayout.component->minimumSize();
		componentLayout.component->setSize(componentSize);

		//If we can't fit it, move it to the next line.
		if (nextX + componentSize.width() > size().width()) {
			nextX = 0;
			nextY = nextLine + _groutY;
		}

		componentLayout.location = Point{ nextX, nextY };
		nextX += _groutX + componentSize.width();
		nextLine = std::max(nextLine, nextY + componentSize.height());
	}
}

Size FlowLayout::minimumSize() {
	return Size();
}