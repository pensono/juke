#ifndef  KNOB_H_DEFINED
#define KNOB_H_DEFINED

#include "unicode/unistr.h"
#include "Component.h"

namespace juke
{
	class Knob : public Component {
	public:
		enum KnobState {
			Inactive,
			Hover,
			Edit,
		};

		Knob(icu::UnicodeString label);
		virtual ~Knob();

		void draw(SkCanvas* canvas) override;
		Size minimumSize() override;

		Proportion value() const { return _value; }

		void doEdit(InputEvent state);

	private:
		Proportion _value;
		icu::UnicodeString label;
	};
}
#endif // include guard