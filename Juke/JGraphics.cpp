#include "JGraphics.h"
#include "Geometry.h"
#include "Colors.h"
#include "SkPaint.h"
#include "SkCanvas.h"

void juke::drawText(SkCanvas* canvas, icu::UnicodeString text, Rect bounds, HorizontalAlignment horizontal, VerticalAlignment vertical) {
	SkPaint textPaint;
	textPaint.setTextSize(16);
	textPaint.setColor(Color::Text);

	Measure x;
	SkPaint::Align textAlignment;
	switch (horizontal) {
	case HorizontalAlignment::Left:
		textAlignment = SkPaint::Align::kLeft_Align;
		x = bounds.left();
		break;
	case HorizontalAlignment::Center:
		textAlignment = SkPaint::Align::kCenter_Align;
		x = bounds.center().x();
		break;
	case HorizontalAlignment::Right:
		textAlignment = SkPaint::Align::kRight_Align;
		x = bounds.right();
		break;
	}	
	textPaint.setTextAlign(textAlignment);
	textPaint.setAntiAlias(true);

	SkPaint::FontMetrics metrics;
	textPaint.getFontMetrics(&metrics);
	Measure textHeight = metrics.fBottom + metrics.fTop;

	Measure y;
	switch (vertical) {
	case VerticalAlignment::Top:
		y = textHeight;
		break;
	case VerticalAlignment::Center:
		y = bounds.top() + (bounds.height() - textHeight) * Half;
		break;
	case VerticalAlignment::Bottom:
		y = bounds.bottom();
	}

	std::string stdString;
	text.toUTF8String(stdString);
	const char* cStr = stdString.c_str();
	canvas->drawText(cStr, strlen(cStr), x, y, textPaint);
}
