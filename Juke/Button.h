#ifndef BUTTON_H_DEFINED
#define BUTTON_H_DEFINED

#include "unicode\unistr.h"
#include "Component.h"
#include "StateMachine.h"

class SkCanvas;

namespace juke
{
	class Button : public Component {
	public:
		enum ButtonState {
			Inactive,
			Hover,
			Pressed,
		};

		Button(icu::UnicodeString label, std::function<void()> action);
		virtual ~Button();

		void draw(SkCanvas* canvas) override;
		Size minimumSize() override;

	private:
		//Properties
		icu::UnicodeString label;
		std::function<void()> action;
	};
}

#endif // ! BUTTON_H_DEFINED