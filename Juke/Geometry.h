#ifndef GEOMETRY_H_DEFINED
#define GEOMETRY_H_DEFINED

#include "SkPoint.h"
#include "SkRect.h"
#include "SkSize.h"
#include "JTypes.h"

/*
 * This is a collection of classes that manage geometry. 
 * It uses the same backing type as skia for maximum compatibility.
 *
 */

namespace juke
{
	class Rect;

	constexpr double PI = 3.1415926535897;

	class Measure {
	private:
		float value;

	public:
		constexpr Measure() : value(0) {}
		constexpr Measure(float value) : value(value) {}
		//constexpr Measure(int value) : value((float) value) {}

		constexpr Measure operator-() const { return Measure{ -value }; }

		constexpr Measure operator <(const Measure b) const noexcept { return value <  b.value; }
		constexpr Measure operator<=(const Measure b) const noexcept { return value <= b.value; }
		constexpr Measure operator >(const Measure b) const noexcept { return value >  b.value; }
		constexpr Measure operator>=(const Measure b) const noexcept { return value >= b.value; }
		constexpr Measure operator==(const Measure b) const noexcept { return value == b.value; }
		constexpr Measure operator!=(const Measure b) const noexcept { return value != b.value; }

		Measure& operator +=(const Measure& b) {
			value += b.value;
			return *this;
		}
		Measure& operator -=(const Measure& b) {
			value -= b.value;
			return *this;
		}

		constexpr operator float() const { return value; }
	};

	constexpr Measure operator-(const Measure a, const Measure b) { return Measure{ (float) a - (float) b }; }
	constexpr Measure operator+(const Measure a, const Measure b) { return Measure{ (float) a + (float) b }; }
	constexpr Measure operator*(const Measure a, const Proportion b)   { return Measure{ (float) a * (float) b }; }
	//constexpr Measure operator/(const Measure a, const Ratio b) { return a * b.inverse(); }
	constexpr Ratio<Measure, Measure> operator/(const Measure a, const Measure b) { return Ratio<Measure, Measure>{ a, b }; }

	//TODO add an angle and bearing(?) class
	
	///Represents a difference (offset) between two points
	class Vector {
	private:
		Measure _x;
		Measure _y;

	public:

		constexpr Measure dx() const { return _x; }
		constexpr Measure dy() const { return _y; }

		constexpr Vector() : _x(), _y() {}
		constexpr Vector(Measure x, Measure y) : _x(x), _y(y) {}
		inline Vector(SkPoint point) : _x(point.x()), _y(point.y()) {}
		inline operator SkPoint() const { return SkPoint::Make(_x, _y); }

		constexpr Vector operator-() const { return Vector{ -_x, -_y }; }

		constexpr Vector operator-(const Vector b) const { return Vector{ _x - b._x, _y - b._y }; }
		constexpr Vector operator+(const Vector b) const { return Vector{ _x + b._x, _y + b._y }; }
	};

	class Point {
	private:
		Measure _x;
		Measure _y;

	public:

		constexpr Measure x() const { return _x; }
		constexpr Measure y() const { return _y; }

		constexpr Point() : _x(), _y() {}
		constexpr Point(Measure x, Measure y) : _x(x), _y(y) {}
		inline Point(SkPoint point) : _x(point.x()), _y(point.y()) {}
		inline operator SkPoint() const { return SkPoint::Make(_x, _y); }

		constexpr Point operator-() const { return Point{ -_x, -_y }; }

		constexpr Vector operator-(const Point b) const { return Vector{ _x - b._x, _y - b._y }; }
		constexpr Vector operator+(const Point b) const { return Vector{ _x + b._x, _y + b._y }; }

		constexpr Point operator+(const Vector b) const { return Point{ _x + b.dx(), _y + b.dy() }; }
		constexpr Point operator-(const Vector b) const { return Point{ _x - b.dx(), _y - b.dy() }; }

		constexpr bool operator==(const Point o) const { return (_x == o._x) && (_y == o._y); }
		constexpr bool operator!=(const Point o) const { return (_x != o._x) || (_y != o._y); }
	};

	class Size {
	private:
		Measure _width;
		Measure _height;

	public:
		constexpr Measure width() const { return _width; }
		constexpr Measure height() const { return _height; }

		constexpr Size() {}
		constexpr Size(Measure width, Measure height) : _width(width), _height(height) {}
		inline Size(SkSize size) : _width(size.width()), _height(size.height()) {}
		inline operator SkSize() { return SkSize::Make(_width, _height); }


		///Returns true if the given rect can fit inside this rect
		constexpr bool fits(const Size b) const {
			return _width > b._width &&
				_height > b._height;
		}

		constexpr Rect centerWithin(const Rect bounds) const;

		constexpr bool contains(const Point point) const {
			return point.x() > Measure{ 0 } && point.x() < _width &&
				point.y() > Measure{ 0 } && point.y() < _height;
		}
	};

	class Rect {
	private:
		Measure _left;
		Measure _top;
		Measure _right;
		Measure _bottom;

	public:
		constexpr Measure width()   const { return _right - _left; }
		constexpr Measure height()  const { return _bottom - _top; }
		constexpr Measure left()    const { return _left; }
		constexpr Measure right()   const { return _right; }
		constexpr Measure top()     const { return _top; }
		constexpr Measure bottom()  const { return _bottom; }
		constexpr Point  location() const { return Point{ _left, _top }; }
		constexpr Point  center()   const { return Point{ _left + width() * Half, _top + height() * Half }; }
		constexpr Size   size()     const { return Size{ width(), height() }; }

		constexpr Rect() {}

		constexpr Rect(const Size size) :
			_left(0), _top(0),
			_right(size.width()), _bottom(size.height()) {}

		constexpr Rect(const Point location, const  Size size) :
			_left(location.x()), _top(location.y()),
			_right(location.x() + size.width()), _bottom(location.y() + size.height()) {}

		constexpr Rect(const Point topLeft, const  Point bottomRight) :
			_left(topLeft.x()), _top(topLeft.y()),
			_right(bottomRight.x()), _bottom(bottomRight.y()) {}

		inline Rect(SkRect rect) :
			_left(rect.left()), _top(rect.top()),
			_right(rect.right()), _bottom(rect.bottom()) {}
		inline operator SkRect() { return SkRect::MakeLTRB(_left, _top, _right, _bottom); }

		constexpr Rect translate(const Vector delta) const { return Rect{ location() + delta, size() }; }
		constexpr Rect inset(const Measure amount) const { return Rect{ Point{_left + amount, _top + amount }, Point{ _right - amount, _bottom - amount} }; }

		constexpr bool contains(const Point point) const { 
			return point.x() > left() && point.x() < right() &&
				point.y() > top() && point.y() < bottom();
		}
	};

	constexpr Rect Size::centerWithin(const Rect bounds) const{
		return Rect{ Point{ bounds.left() + (bounds.width() - _width) * Half,
			bounds.top() + (bounds.height() - _height) * Half }, *this };
	}

	class Angle {
		float angleDegrees;

	public:
		constexpr Angle(float angleDegrees) : angleDegrees(angleDegrees) {}
		constexpr operator float() const { return angleDegrees; }
		constexpr Angle operator -(const Angle other) const { return Angle{ angleDegrees - other.angleDegrees }; }
		constexpr Angle operator +(const Angle other) const { return Angle{ angleDegrees + other.angleDegrees }; }
	};
	
	constexpr Angle operator*(const Angle a, const Proportion b) { return Angle{ (float)a * (float)b }; }

	constexpr Angle operator "" _deg(long double angleDegrees) { return Angle{ (float) angleDegrees }; };
	constexpr Angle operator "" _deg(unsigned long long angleDegrees) { return Angle{ (float)angleDegrees }; };
	constexpr Angle operator "" _rad(long double angleRadians) { return Angle{ (float) (angleRadians * PI / 180.0f) }; }
	constexpr Angle operator "" _rad(unsigned long long angleRadians) { return Angle{ (float)(angleRadians * PI / 180.0f) }; }
}
#endif
