#ifndef COMPONENT_H_DEFINED
#define COMPONENT_H_DEFINED

#include "InputState.h"
#include "StateMachine.h"

class SkCanvas;

namespace juke
{
	enum class FocusDirection {
		Forward, Reverse
	};

	class Component {
	public:
		Component(StateMachine machine);
		virtual ~Component();

		Size size() { return _size; }
		void setSize(Size newSize) {
			//SkASSERT(canContain(newSize,getMinimumSize());
			_size = newSize;
			onSizeChange();
		}

		virtual void draw(SkCanvas* canvas) = 0;
		virtual Size minimumSize() = 0;
		
		///Send the event to the correct handler
		/**
		 * You may override this if you wish to handle all mouse events in one function.
		 */
		virtual bool dispatchMouseEvent(InputEvent event) {
			_stateMachine.dispatchEvent(event);
			return false; 
		}

		Component* parent() { return _parent; }
		void attachParent(Component* parent) { _parent = parent; }

		bool isFocused() { return _focused; }
		virtual void setFocused(bool focus);

		///Returns the leaf node with focus
		/**
		 * May return nullptr.
		 */
		virtual Component* focusedComponent();

		//virtual void advanceFocus(FocusDirection);

		StateMachine* stateMachine() { return &_stateMachine; }


	protected:
		virtual void onSizeChange() {}
		virtual void onFocusChange() {}

	private:
		Size _size;
		Component* _parent;
		bool _focused;
		StateMachine _stateMachine;
	};
}
#endif //include guard