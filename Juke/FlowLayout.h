#ifndef FLOWLAYOUT_H_DEFINED
#define FLOWLAYOUT_H_DEFINED

#include "Geometry.h"
#include "Layout.h"

namespace juke
{
	class FlowLayout : public Layout {
	public:
		FlowLayout();
		~FlowLayout();
		
		void layoutComponents() override;
		Size minimumSize() override;

		Measure groutX() { return _groutX; }
		void setGroutX(Measure newGroutX) { _groutX = newGroutX; }
		Measure groutY() { return _groutY; }
		void setGroutY(Measure newGroutY) { _groutY = newGroutY; }

	private:
		Measure _groutX = 4;
		Measure _groutY = 4;
	};
}

#endif //include guard