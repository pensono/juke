#ifndef ENABLE_BITMASK_OPERATORS_H
#define ENABLE_BITMASK_OPERATORS_H

template<typename E>
struct enable_bitmask_operators {
	static constexpr bool enable = false;
};

template<typename E>
constexpr typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type
operator|(E lhs, E rhs) {
	typedef typename std::underlying_type<E>::type underlying;
	return static_cast<E>(static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
};

template<typename E>
constexpr typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type
operator&(E lhs, E rhs) {
	typedef typename std::underlying_type<E>::type underlying;
	return static_cast<E>(static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
};

template<typename E>
constexpr
bool test(E arg, E position,
	typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type* = 0) {
	typedef typename std::underlying_type<E>::type underlying;
	return (static_cast<underlying>(arg) & static_cast<underlying>(arg)) != 0;
};

template<typename E>
constexpr typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type
set(E arg, E position) {
	typedef typename std::underlying_type<E>::type underlying;
	return static_cast<E>(
		static_cast<underlying>(arg) | static_cast<underlying>(position));
};

template<typename E>
constexpr typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type
clear(E arg, E position) {
	typedef typename std::underlying_type<E>::type underlying;
	return static_cast<E>(
		static_cast<underlying>(arg) & ~static_cast<underlying>(position));
};

template<typename E>
constexpr typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type
operator~(E arg) {
	typedef typename std::underlying_type<E>::type underlying;
	return static_cast<E>(~static_cast<underlying>(arg));
};

#endif // Include guard
