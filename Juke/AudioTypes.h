#ifndef AUDIO_TYPES_H_DEFINED
#define AUDIO_TYPES_H_DEFINED

#include <cstdint>
#include <functional>
#include <algorithm>
#include <math.h>

namespace juke
{
	class Phase {
		uint16_t value;
	};


	// Please SIMDify from this point down once Vc supports MSVC
	// Non Vc version needs optimization as well
	template<uint16_t N>
	class MaskStream {
	private:
		bool data[N];
	};

	template<uint16_t N>
	class FloatStream {
	private:
		void mapInPlace(std::function<float(float)> operation) {
			for (int i = 0; i < N; i++) {
				data[i] = operation(data[i]);
			}
		}

	public:
		FloatStream<N> map(std::function<float(float, float)> operation, FloatStream other) const {
			FloatStream<N> result;
			for (int i = 0; i < N; i++) {
				result[i] = operation(data[i], other[i]);
			}
			return result;
		}

		FloatStream<N> map(std::function<float(float)> operation) const {
			FloatStream<N> result;
			for (int i = 0; i < N; i++) {
				result[i] = operation(data[i]);
			}
			return result;
		}

		static FloatStream<N> IndexesFromZero() {
			FloatStream<N> result;
			for (int i = 0; i < N; i++) {
				result[i] = (float) i;
			}
			return result;
		}

		constexpr MaskStream<N> operator <(const FloatStream<N> other) const { return map((a, b) { return a <  b; }, other); }
		constexpr MaskStream<N> operator<=(const FloatStream<N> other) const { return map((a, b) { return a <= b; }, other); }
		constexpr MaskStream<N> operator >(const FloatStream<N> other) const { return map((a, b) { return a >  b; }, other); }
		constexpr MaskStream<N> operator>=(const FloatStream<N> other) const { return map((a, b) { return a >= b; }, other); }
		constexpr MaskStream<N> operator==(const FloatStream<N> other) const { return map((a, b) { return a == b; }, other); }
		constexpr MaskStream<N> operator!=(const FloatStream<N> other) const { return map((a, b) { return a != b; }, other); }

		constexpr FloatStream<N> operator+(const FloatStream<N> other) const { return map((a, b) { return a +  b; }, other); }
		constexpr FloatStream<N> operator-(const FloatStream<N> other) const { return map((a, b) { return a -  b; }, other); }
		constexpr FloatStream<N> operator*(const FloatStream<N> other) const { return map((a, b) { return a *  b; }, other); }
		constexpr FloatStream<N> operator/(const FloatStream<N> other) const { return map((a, b) { return a /  b; }, other); }
		constexpr FloatStream<N> operator%(const FloatStream<N> other) const { return map((a, b) { return std::fmodf(a, b); }, other); }

		void operator+=(const float b) { return mapInPlace([b](const float item) { return item + b; }); }
		void operator-=(const float b) { return mapInPlace([b](const float item) { return item - b; }); }
		void operator*=(const float b) { return mapInPlace([b](const float item) { return item * b; }); }
		void operator/=(const float b) { return mapInPlace([b](const float item) { return item / b; }); }
		void operator%=(const float b) { return mapInPlace([b](const float item) { return std::fmodf(item, b); }); }

		//constexpr float  operator[](std::size_t index) const { return data[index]; }
		float& operator[](std::size_t index) { return data[index]; }
	private:
		float data[N];
	};

	template<size_t N> constexpr FloatStream<N> operator+(const FloatStream<N> a, const float b) { return a.map([b](float item) { return item + b; }); }
	template<size_t N> constexpr FloatStream<N> operator-(const FloatStream<N> a, const float b) { return a.map([b](float item) { return item - b; }); }
	template<size_t N> constexpr FloatStream<N> operator*(const FloatStream<N> a, const float b) { return a.map([b](float item) { return item * b; }); }
	template<size_t N> constexpr FloatStream<N> operator/(const FloatStream<N> a, const float b) { return a.map([b](float item) { return item / b; }); }
	template<size_t N> constexpr FloatStream<N> operator%(const FloatStream<N> a, const float b) { return a.map([b](float item) { return std::fmodf(item, b); }); }

	template<size_t N> constexpr FloatStream<N> operator+(const float a, const FloatStream<N> b) { return b.map([a](float item) { return a + item; }); }
	template<size_t N> constexpr FloatStream<N> operator-(const float a, const FloatStream<N> b) { return b.map([a](float item) { return a - item; }); }
	template<size_t N> constexpr FloatStream<N> operator*(const float a, const FloatStream<N> b) { return b.map([a](float item) { return a * item; }); }
	template<size_t N> constexpr FloatStream<N> operator/(const float a, const FloatStream<N> b) { return b.map([a](float item) { return a / item; }); }
	template<size_t N> constexpr FloatStream<N> operator%(const float a, const FloatStream<N> b) { return b.map([a](float item) { return std::fmodf(a, item); }); }
}

#endif // !AUDIO_TYPES_H_DEFINED
