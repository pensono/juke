#ifndef STATE_MACHINE_H_DEFINED
#define STATE_MACHINE_H_DEFINED

#include <functional>
#include <vector>
#include <utility>
#include "InputState.h"

namespace juke
{
	class Component;

	using Event = std::function<bool(InputEvent)>;

	extern Event MouseDown(MouseButton button);
	extern Event MouseUp(MouseButton button);
	extern Event MouseEnter(Component* component);
	extern Event MouseExit(Component* component);
	extern Event MouseInside(Component* component);
	extern Event MouseMove;

	template<typename First, typename... Rest>
	Event And(First a, Rest... rest) {
		Event b = And(rest...);
		return [a, b]
			(InputEvent event) -> bool {
			return a(event) && b(event);
		};
	}

	template<typename First>
	Event And(First a) {
		return a;
	}

	template<typename First, typename... Rest>
	Event Or(First a, Rest... rest) {
		Event b = Or(rest...);
		return [a, b]
			(InputEvent event) -> bool {
			return a(event) || b(event);
		};
	}

	template<typename First>
	Event Or(First a) {
		return a;
	}

	extern Event Not(Event event);

	class StateTransition {
	public:
		StateTransition(Event trigger, int destinationState, std::function<void(InputEvent)> action = [](InputEvent c){});
		 
		const Event trigger() { return _trigger; }
		const int destinationState() { return _destinationState; }
		const std::function<void(InputEvent)> action() { return _action; }

	private:
		Event _trigger;
		std::function<void(InputEvent)> _action;
		int _destinationState;
	};

	class State {
	public:
		State(int state, std::vector<StateTransition> transitions);

		const int state() { return _state; }
		const std::vector<StateTransition> transitions() { return _transitions; }

	private:
		std::vector<StateTransition> _transitions;
		int _state;
	};

	class StateMachine {
	public:
		StateMachine(int initialState, std::vector<State> states);
		//~StateMachine();

		void dispatchEvent(InputEvent event);
		int currentState() { return _currentState; }

	private:
		int _currentState;
		std::vector<State> _states;
	};
}
#endif // !STATE_MACHINE_H_DEFINED
