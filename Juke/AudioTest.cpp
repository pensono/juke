//#include "Vc/Vc"
#include "AudioTest.h"
#include <iostream>
#include "RtAudio.h"
#include "AudioTypes.h"

AudioTest::AudioTest() {}

AudioTest::~AudioTest() {}

int saw(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
	double streamTime, RtAudioStreamStatus status, void *userData) {
	float *buffer = (float *)outputBuffer;
	std::cout << "Streaming";
	if (status)
		std::cout << "Stream underflow detected!" << std::endl;

	//Generate the saw wave on a 0..1 scale, then translate to -1..1 when writing to audio
	
	auto currentPosition = (float*)userData;

	auto time = juke::FloatStream<256>::IndexesFromZero();

	float periodS = 1 / 440.0f;
	float periodSamples = periodS * 44100.0f;

	float step = 1 / periodSamples; // Increment per sample

	auto samples = (time * step + *currentPosition) % 1.0f;
	*currentPosition += 256 * step;

	//Transform from 0..1 to -1..1
	samples *= 2.0;
	samples -= 1.0;

	//Write to output buffer
	for (int i = 0; i < 256; i++) {
		buffer[i * 2] = samples[i];
		buffer[i * 2 + 1] = samples[i];
	}

	return 0;
}


void AudioTest::start() {
	// Direct Sound is great for development, because you can have other 
	// audio devices (music players) running in the background while you code
	RtAudio audio { RtAudio::Api::WINDOWS_DS };
	// Determine the number of devices available
	unsigned int devices = audio.getDeviceCount();
	// Scan through devices for various capabilities
	RtAudio::DeviceInfo info;
	for (unsigned int i = 0; i<devices; i++) {
		info = audio.getDeviceInfo(i);
		if (info.probed == true) {
			// Print, for example, the maximum number of output channels for each device
			std::cout << info.name << ":\n";
			std::cout << "  Maximum output channels = " << info.outputChannels << "\n";
			std::cout << "  Maximum input channels = " << info.inputChannels << "\n";
			std::cout << "  Duplex channels = " << info.duplexChannels << "\n";
		}
	}

	RtAudio::StreamParameters parameters;
	parameters.deviceId = audio.getDefaultOutputDevice();
	parameters.nChannels = 2;
	parameters.firstChannel = 0;
	unsigned int sampleRate = 44100;
	unsigned int bufferFrames = 256; // 256 sample frames
	float data[1];
	try {
		audio.openStream(&parameters, NULL, RTAUDIO_FLOAT32,
			sampleRate, &bufferFrames, &saw, (void *)&data);
		audio.startStream();

		std::cout << "Streaming audio\n";
	} catch (RtAudioError& e) {
		e.printMessage();
	}

	char input;
	std::cout << "\nPlaying ... press <enter> to quit.\n";
	std::cin.get(input);
	try {
		// Stop the stream
		audio.stopStream();
	} catch (RtAudioError& e) {
		e.printMessage();
	}
	if (audio.isStreamOpen()) audio.closeStream();
}