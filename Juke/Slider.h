#ifndef SLIDER_H_DEFINED
#define SLIDER_H_DEFINED

#include "JTypes.h"
#include "Component.h"
#include "unicode\unistr.h"

class SkCanvas;

namespace juke
{
	class Slider : public Component {
	public:
		enum SliderState {
			Inactive,
			Hover,
			Edit,
		};

		Slider(icu::UnicodeString label);
		virtual ~Slider();

		void draw(SkCanvas* canvas) override;
		Size minimumSize() override;

		void doEdit(InputEvent ev);

	private:
		Proportion value = Half;

		icu::UnicodeString label;

		//Constants
		static constexpr Measure bottomHeight		= Measure{ 12 };
		static constexpr Measure sliderHeight		= Measure{ 8 };
		static constexpr Measure trackWidthActive	= Measure{ 4 };
		static constexpr Measure trackWidthInactive	= Measure{ 3.25 };
	};
}
#endif //Include Guard