#include <algorithm>
#include "Component.h"
#include "StateMachine.h"

using namespace juke;

// Events
Event juke::MouseDown(MouseButton button){
	return [button](InputEvent event) {
		return (event.now().buttonPressed(button) && !event.then().buttonPressed(button));
	};
};

Event juke::MouseUp(MouseButton button) {
	return [button](InputEvent event) {
		return (!event.now().buttonPressed(button) && (event.then().buttonPressed(button)));
	};
};

Event juke::MouseEnter(Component* component) {
	return [component](InputEvent event) {
		return (component->size().contains(event.now().location())) &&
			!(component->size().contains(event.then().location()));
	};
};

Event juke::MouseExit(Component* component) {
	return [component](InputEvent event) {
		return !(component->size().contains(event.now().location())) &&
			(component->size().contains(event.then().location()));
	};
}

Event juke::MouseInside(Component* component) {
	return [component](InputEvent event) {
		return component->size().contains(event.now().location());
	};
}

Event juke::MouseMove = [](InputEvent event) {
	return event.now().location() != event.then().location();
};

Event juke::Not(Event event) {
	return [=](InputEvent input) { return !event(input); };
}


//Constructors
StateMachine::StateMachine(int initialState, std::vector<State> states) :
	_currentState(initialState), _states(states) {
}

void StateMachine::dispatchEvent(InputEvent event) {
	auto currentState = std::find_if(_states.begin(), _states.end(), [this](State& s) -> bool { return s.state() == _currentState; });

	for (auto transition : currentState->transitions())	{
		if (transition.trigger()(event)) {
			_currentState = transition.destinationState();
			transition.action()(event);
			break;
		}
	}
}


State::State(int state, std::vector<StateTransition> transitions) :
	_state(state), _transitions(transitions){
}



StateTransition::StateTransition(Event trigger, int destinationState, std::function<void(InputEvent event)> action):
	_trigger(trigger), _destinationState(destinationState), _action(action) {
}
