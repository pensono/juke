#include <algorithm>
#include "Layout.h"
#include "SkCanvas.h"

using namespace juke;


Layout::Layout():
	Component(StateMachine{ 0, {
		State{0,{
			//StateTransition{MouseDown(MouseButton::Left), 0, [&](MouseEvent c) { setFocused(true);}}
		} }
	} }) {}

Layout::~Layout() {}

bool Layout::dispatchMouseEvent(InputEvent event) {
	for (auto& componentLayout : children()) {
		Vector offset = Vector{ -componentLayout.location };
		componentLayout.component->dispatchMouseEvent(event.translate(offset));
	}

	Component::dispatchMouseEvent(event);

	return true;
}

Component* Layout::focusedComponent() {
	for (auto& componentLayout : children()) {
		if (componentLayout.component->isFocused())
			return componentLayout.component->focusedComponent();
	}

	//Cannot return this in accordance with function spec to return only leaf nodes
	return nullptr; 
}

void Layout::setFocused(bool focused) {
	Component::setFocused(focused);
	for (auto& componentLayout : children()) {
		componentLayout.component->setFocused(false);
	}
}
//void Layout::advanceFocus(FocusDirection direction) {
//	auto it = children().begin();
//
//	while (it != children().end()) {
//		if (it->component->isFocused())
//			break;
//		it++;
//	} //it now points to the focused component
//
//
//}

void Layout::addChild(std::unique_ptr<Component> component) {
	component->attachParent(this);
	components.push_back(ComponentLayout{ std::move(component), Point{} });
	onChildAdded();
}

void Layout::draw(SkCanvas* canvas){
	for (auto& layoutComponent : children()) {
		canvas->save();
		canvas->translate(layoutComponent.location.x(), layoutComponent.location.y());
		layoutComponent.component->draw(canvas);
		canvas->restore();
	}
}

